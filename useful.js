
//checks for any objects in the array (myArr) that contain the same object property (prop input as a string)
const removeDuplicateObjects = (myArr, prop) => {
    //checks that the prop given is actually in the first object -> no point comparing objects that don't have the propertiy being compared.
    if(myArr[0].hasOwnProperty(prop)){
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }else{
        console.log(`Objects do not contain property: ${prop} In removeDuplicates function`);
        return myArr;
    }   
}

const removeDuplicateValues = (myArr) => {
    let uniqueValues = [...new Set(myArr)];
    return uniqueValues;
}


const removeObjectsWithArray = (a, obseleteProperties) => {
    let i, b = [];
    for(i = 0; i < obseleteProperties.length; i++){
        b[i] = new Object({link:obseleteProperties[i]});
    }
    //returns an array of items that are not in both arrays
    let c = a.filter(item => !b.some(other => item.link === other.link));
    //console.log(c); 
    return c;
}

const removeObjectsWithObjects = (a, b) => {
   
    //returns an array of items that are not in both arrays
    let c = a.filter((item) => {
        return !b.some((other) => {
            return item.auctionID === other.auctionID
        })
    });
    //console.log(c); 
    return c;
}

module.exports = {
    removeDuplicateObjects,
    removeDuplicateValues,
    removeObjectsWithArray,
    removeObjectsWithObjects
}


const testLinks = ["http://b-net.tackleberry.co.jp/ec/stk/stk_detail.cfm?stkNo=72721230045673788901231K&shopID=352698ZY76634000059X7&keyS=335111NW108893001RMGU&key1=7833307U31110816002564L6&type=a", "http://b-net.tackleberry.co.jp/ec/stk/stk_detail.cfm?stkNo=72721230045673788901231K&shopID=352698ZY76634000059X7&keyS=335111NW108893001RMGU&key1=7833307U31110816002564L6&type=a", ];