var cheerio = require('cheerio');
var request = require('request-promise');

const format = require('./formatData');
const scraper = require('./scrapeFunctions');

const rootLink = 'https://auctions.yahoo.co.jp/category/list/%E9%9B%BB%E5%8B%95-%E8%88%B9%E7%94%A8-%E3%83%AA%E3%83%BC%E3%83%AB-%E3%83%95%E3%82%A3%E3%83%83%E3%82%B7%E3%83%B3%E3%82%B0-%E3%82%B9%E3%83%9D%E3%83%BC%E3%83%84-%E3%83%AC%E3%82%B8%E3%83%A3%E3%83%BC/2084203245/?select=03&fr=auc-prop&tab_ex=commerce&p=%E9%9B%BB%E5%8B%95';

const baseUrl = false;
const pageLinkSelector = 'a[class="n"]';

const linkSelectors = {
    general:'#list01 > table > tbody > tr', //general listing component
    link : `> td.a1 > div > h3 > a`,
    auctionID : `> td.i > div > table > tbody > tr > td > a > img`,
    price : ` > td.pr1`,
    title : `> td.a1 > div > h3 > a` 
 };  

const dataSelectors =  {
    title:'h1[class="ProductTitle__text"]',
    imageUrls: 'div[class="ProductImage__inner"]',
    endDate: '#l-main > div > div.ProductDetail > div > div > div.l-left > ul > li:nth-child(4) > dl > dd',
    startDate: false,
    currentPrice: '#l-sub > div.ProductInformation > ul > li.ProductInformation__item.js-stickyNavigation-start > div.Price.Price--current > dl > dd.Price__value',
    buyNowPrice: '#l-sub > div.ProductInformation > ul > li.ProductInformation__item.js-stickyNavigation-start > div.Price.Price--buynow > dl > dd.Price__value',
    auctionID: '#l-main > div > div.ProductDetail > div > div > div.l-right > ul > li:nth-child(6) > dl > dd > span',
    rating: false,
    store: 'Yahoo',
    productDescription: 'div[class="ProductExplanation__body highlightWordSearch"]'  
};

const updateYahoo = async (oldData = []) => {
    try{
    //Enter site specific requests to the scraper system
    let newData = await scraper.update(rootLink, oldData, pageLinkSelector, linkSelectors, baseUrl, requestPageData);
    return newData;
    }catch(e){
        console.log('Had some issue: ', e);
    }
}


//--------------------------------------------------------------
//Requests five listing pages for data at a time (or less than five)
const requestPageData = async (fiveTempLinks) => {
    let startDate, endDate, rating;
    //const browser = await puppeteer.launch();
    const tempData = await Promise.all(fiveTempLinks.map(async (linkData) =>{
        try{    
            const pageHTML = await request(linkData.link);
            const $ = cheerio.load(pageHTML);

            //Product title Box
            const tempJapanese = $(dataSelectors.title).text()
            const tempEnglish = await format.translateJPN(tempJapanese);
            const title = format.cleanString(tempEnglish);

            //Product image URLs
            const imageUrls = $(dataSelectors.imageUrls).map((i, url) => {
                return ($(url).find('img').attr('src'));
            }).get();
            
            //Product current price
            let tempEnglish4 = $(dataSelectors.currentPrice).text();
            const currentPrice = format.processCurrency(tempEnglish4);

            //Product buy now price
            let tempEnglish2 = $(dataSelectors.buyNowPrice).text();
            const buyNowPrice = format.processCurrency(tempEnglish2);
            
            //Product  start Date
            startDate = new Date().toString();

            //Product end Date
            endDate = $(dataSelectors.endDate).text();

            //Product auction ID
            const auctionID = `${linkData.auctionID}`;
            
            //Product Description Text
            const tempJapanese2 = $(dataSelectors.productDescription).text();
            //console.log(tempJapanese);
            const tempEnglish3 = await format.translateJPN(tempJapanese2);
            const productDescription = format.cleanString(tempEnglish3);

            //Product Rating
            rating = $(dataSelectors.rating).attr('src');

            //Get Listing time
            let temp = {
                    title: title,
                    linkData: linkData,
                    imageUrls: imageUrls,
                    endDate: endDate,
                    startDate: startDate,
                    currentPrice: currentPrice,
                    buyNowPrice: buyNowPrice,
                    auctionID: auctionID,
                    rating: rating,
                    store: 'Yahoo',
                    productDescription: productDescription
            };
            return temp;
        }catch (e){
            console.log('error is when trying to load individual listing pages:', e);
            return;
        }
    }));
    //console.log(tempData);
    return tempData;
}

// ------------------------------------------------------------
module.exports = {
    updateYahoo
}
