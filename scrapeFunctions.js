var request = require('request-promise');
var cheerio = require('cheerio');
const puppeteer = require('puppeteer');
const fs = require('fs-extra');

const tools = require('./useful');

//--------------getHTML is a function that uses Puppeteer to scrape site data from dynamic sites
const getSingleHTML = async (link) => {
    try {
        const browser = await puppeteer.launch({
            args: ['--no-sandbox']
          });
        const page = await browser.newPage();
        await page.setRequestInterception(true);
        page.on('request', (request) => {
            if (['image', 'stylesheet', 'font', 'script'].indexOf(request.resourceType()) !== -1) {
                request.abort();
            } else {
                request.continue();
            }
        });
        await page.goto(link, { waitUntil: 'networkidle2'});
        let html = await page.content();
        browser.close();
        return html;
    } catch (e) {
        return e;
    }
}
// ---------------Check For More Pages ---------------------------------------------

//This function takes an array of the current page number links and clicks on the last page in the array, opening a new page with new page links. It then looks for any pages that it doesnt already have. It then adds any new page number links to the current array of page number links and returns the new array (the dependant function checks for a change in size to work ou when there are no more pages). 'a[class="n"]' is the selector for Yahoo

const checkForMorePages = async (currentPageNumberLinks, pageLinkSelector, baseUrl) => {
    let addLinks = [];
    const thisLink = currentPageNumberLinks[currentPageNumberLinks.length-1];

    try{
        //const html = await request(`${thisLink}`);
        const html = await getSingleHTML(`${thisLink}`);
        $ = cheerio.load(html);

        //get all the pagination links on the current page (should be about 10 links)
        let newLinks = $(pageLinkSelector).map((index, element)=>{
            return $(element).attr('href'); 
        }).get();

        //remove duplicates
        newLinks = [...(new Set(newLinks))];
        if(baseUrl){
            newLinks = newLinks.map(el => `${baseUrl}${el}`);
        }

        //only get the links we dont already have
        addLinks = newLinks.filter( (link) => {
            return !(currentPageNumberLinks.includes(link));
        })
        //Add the new links found (most pages will only have 1 or 2 additional page links)
        return [...currentPageNumberLinks, ...addLinks];
    
    }catch(err){
        return err;
    }
}

//------------------------Function: PageOfListings-----------------------------
// It returns an array of page Number links. It initialises the CheckForMorePagesfunction to get all page links first...
const getPageNumberLinks = async (startLink, pageLinkSelector, baseUrl) => {
    let $, currentLength, cont = true, newPageNumberLinks ;
    //Find all of the visible page links
    try{
        //const mainHtml = await request(startLink);
        const mainHtml = await getSingleHTML(startLink);
        $ = cheerio.load(mainHtml);

        //Get all pagination links on the current page
        let pageNumberLinks = $(pageLinkSelector).map((index, element)=>{
            let tempLink = $(element).attr('href');
            return tempLink; 
        }).get();

        //remove duplicates
        pageNumberLinks = [...(new Set(pageNumberLinks))];
        if(baseUrl){
            pageNumberLinks = pageNumberLinks.map(el => `${baseUrl}${el}`)
        }
        currentLength = pageNumberLinks.length;
        newPageNumberLinks = await checkForMorePages(pageNumberLinks, pageLinkSelector, baseUrl);

        //if the checkPages returns true then redo
        while (cont){
            newPageNumberLinks = await checkForMorePages(newPageNumberLinks, pageLinkSelector, baseUrl);
            if(newPageNumberLinks.length === currentLength)
            {
                //if the returned array is no longer than the array we sent then there are no more pages of listings
                cont = false;
            }else{
                currentLength = newPageNumberLinks.length;
            }
        }
        //return the list of page number links
        return newPageNumberLinks;
    }catch(e){
        return e;
    }
};

//--------------------------------All Listings on a single page without going into them---------
let getAllListingLinks = async (pageNumberLinks, linkSelectors) => {
    let combinedListings = [], listingData =[];
    try{
        listingData = await Promise.all(pageNumberLinks.map(async (pageNumberLinkI) =>{
            try{ 
                //get all links for listings (20 links) for the pagelinks link      
                const pageHtml = await request(pageNumberLinkI);

                //const pageHtml = await getSingleHTML(pageNumberLinkI);
                const $ = cheerio.load(pageHtml);
                
                //get all links for listings (50 links) for the pagelinks link
                let links = $(linkSelectors.general).map((index, el) => {
                    let link = $(el).find(linkSelectors.link).attr('href');
                    let auctionID = $(el).find(linkSelectors.auctionID).attr('src');
                    let price = $(el).find(linkSelectors.price).text();
                    let title = $(el).find(linkSelectors.title).text();
                    title = title.replace(/[\t\n\r]/gm,'');
                    let comment2 = 'comment 2';
                    if(!link || !auctionID){
                        return;
                    }
                    return  {
                        link,
                        auctionID:`${auctionID}${title}`,
                        price,
                        comment2
                    }
                }).get();
                return links;
            }
            catch(e){
                return e;
            }
        }));
        console.log('s');
        
        for (let a = 0; a < pageNumberLinks.length; a ++ ){
            combinedListings = [...combinedListings, ...listingData[a]];
        }
        //remove duplicates
        listingData = tools.removeDuplicateObjects(combinedListings, 'auctionID');
        return listingData; 
    }catch(e){
        return e;
    }
}

// -------------------------------Page Scraper------------------------------
//This function takes an array of listing page links and collects individual page data
let getPageData = async (pageLinks, requestPageDataCallBack) =>{ 
    
    let total = pageLinks.length;// only used for console log
    let newData = [];

    //If we get lots of links then get them 5 pages at a time
    if(pageLinks.length >= 5){
        let counter = 0;
        while (pageLinks.length >= 5){
            let fiveTempLinks = [pageLinks[0], pageLinks[1], pageLinks[2], pageLinks[3], pageLinks[4]];
            //Simultaneously collects all page data (a promise per page is created)
            let tempData = await requestPageDataCallBack(fiveTempLinks);
            console.log(`Scraped data for ${counter + 1} - ${counter + 5} of ${total} listings.`);
            newData = [...newData, ...tempData];
            pageLinks.splice(0, 5);
            fs.writeFileSync('./tempData.json', JSON.stringify(newData));//temp for if it crashes;
            //pageLinks = 0;
            counter = counter + 5;
        }
    }  

    //Otherwise if we have less than 5 links (from start, or left over) just get all at once
    if((pageLinks.length < 5) && (pageLinks.length > 0)){
         console.log(`Only ${pageLinks.length} pages left to search.`);
         let tempData = await requestPageDataCallBack(pageLinks);
         newData = [...newData, ...tempData];
    } 
    return newData;
}

//----------------------Update the listings--------------------------
const update = async (rootLink , oldData = [], pageLinkSelector , linkSelectors, baseUrl = '', requestPageDataCallBack) => {

    let i,  oldLinkData = [], uniqueData = [], newData = [], newLinkData = [], currentLinkData = [], obseleteLinkData = [];
    
    if(oldData.length > 1){
        for(i = 0; i < oldData.length; i++){
            oldLinkData[i] = oldData[i].linkData; 
            }
    }

    try{
        if(typeof(oldData) !== 'object'){throw 'Incorrect Old Data Argument';}

        //Get all page number links from the root section until it goes out. 
        const allPageLinks = await getPageNumberLinks(rootLink, pageLinkSelector, baseUrl);

        //Get a list of all Listings (now link data)
        currentLinkData = await getAllListingLinks(allPageLinks, linkSelectors);

        //if any links in currentLinkData is in the OldLinkData then remove it from the newLinkData Array
        if((typeof(currentLinkData) === 'object') && (currentLinkData.length)){
            newLinkData = currentLinkData.filter(obj1 => oldLinkData.every(obj2 => obj1.auctionID !== obj2.auctionID));
            obseleteLinkData = oldLinkData.filter(obj1 => currentLinkData.every(obj2 => obj1.auctionID !== obj2.auctionID));
        }
       
        //remove any old links that were obselete (not in the new live listings list)
        oldData = tools.removeObjectsWithObjects(oldData, obseleteLinkData);
        
        console.log(`Getting ${newLinkData.length} New Listings`);
        
        //process new listings and output new data
        newData = await getPageData(newLinkData, requestPageDataCallBack);

        //then remove any duplicates based on auctionIDs - we want to keep the old data over the new data to save the start date
        const prior = oldData.length + newData.length;
        uniqueData = tools.removeDuplicateObjects([...oldData,...newData], 'auctionID');
        const after = uniqueData.length;
        console.log(`There were ${prior-after} listings that were missed in link duplicate removal`);

        //combine old and new data
        return uniqueData;
    }catch(e){
        console.log('Unable to update because of an issue with Yahoo scraping functions: ', (e));
        return oldData;
    }
}

const deleteOldListings = async (data) => {

}

module.exports = {
    update,
    deleteOldListings,
    getAllListingLinks
}