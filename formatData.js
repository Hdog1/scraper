//Used to process Japanese raw data
const translate = require('translate');
const fs = require('fs-extra');

//Listing constructor that keeps data consistent
class Listing {
    constructor (title, link, endDate, startDate, currentPrice, buyNow , auctionID, description, imageUrls, store, rating ='N') {
        this.title = title;
        this.link = link;
        this.endDate = endDate;
        this.startDate = startDate;
        this.currentPrice = currentPrice;
        this.buyNowPrice = buyNow;
        this.auctionID = auctionID;
        this.description = description;
        this.imageUrls = imageUrls;
        this.store = store;
        this.rating = rating;
    }
}

//--------------------async function that translates japanese to english
const translateJPN = async (japanese) => {
    
    const options = { from: 'ja', to: 'en', engine: 'yandex', key: 'trnsl.1.1.20181114T172647Z.c590ae463e418f94.09aeb20378745fe3997d9cf205ff20a4da59d5d4' };
    
    if (japanese.length > 300) {
        japanese = japanese.substring(0,300);
    }
    try{
        let english = await translate(japanese.trim(), options);
        return english;
    } catch(e){
        console.log('Translation Error');
        return `Yandex ${japanese}`;
    }
}; 

//----------------------------Remove none english characters and white space
var cleanString = (str) => {
    let tidied =  str.replace(/[\W_]+/g, " ").trim();
    return tidied;
}

//------------------------Load data from a file---------------------------
var loadOldData = async (file) => {
    //check that the file already exists.   
    try{
        //console.log('trying this', file);
        var existingData = await fs.readFileSync(file);
       // console.log('Data extracted'); 
        return JSON.parse(existingData);
    } catch(error){
        //file does not exist
       console.log('Unable to loadOldData() - no file or no data?'); 
       return false;
    }
};

//------------------------------Save to file---------------
var saveData = (data , file) => {
    if(data === null || typeof(data) !== 'object'){
        console.log(`Dodgy Data trying to be saved ${data}`);
        return;
    }
    console.log(`${data.length} listings saved in ${file}`);
    fs.writeFileSync(file, JSON.stringify(data));
};
// -----------------------------delete file
var deleteData = (file) => {
    fs.unlink(file,  (err) => {
        if (err) throw err;
        console.log('File deleted!');
      });
};


//-----------------------------formats currency to numbers--------------
//Takes any string and spits max number out - returns 9999999 for input with no numbers or no string input
const processCurrency = (rawCurrency) => {
    if(typeof(rawCurrency) === 'string'){
    // separate words by ' ', remove non digits, check that they can be converted to numbers, then convert valid strings to numbers
    let numbers = (rawCurrency.trim().split(' ').map(element => element.replace(/[^0-9]/g, '')).filter(element => !isNaN(parseInt(element))));
        if(numbers.length){
            let processedNumbers = numbers.map(num => parseInt(num,10));
            //Return the max number
            return  Math.max(...processedNumbers);
        }
        return `-- ${rawCurrency}`;
    }
    return `Argunment needs to be a string but was: ${rawCurrency}`;
}

module.exports = {
    translateJPN,
    cleanString,
    loadOldData,
    saveData,
    deleteData,
    processCurrency
}
