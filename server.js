// npm exports
var express = require('express');
var bodyParser = require('body-parser');
const cron = require("node-cron");
// const fs = require('fs-extra');

const tools = require('./useful');
const format = require('./formatData');
const yahoo = require('./scrapeYahoo');
const tackleberry = require('./scrapeTackleBerry');


require('events').EventEmitter.defaultMaxListeners = 1000;

const cleanFile = './tbFile.json', tempFile = './tempData.json';

let data = [], yahooData = [], tackleberryData = [], initialised = false, firstUpdate = false;

//scrape data on startup async wrapper function
const initData = async () => {
    try{
        let tempData = await format.loadOldData(cleanFile);
        //let tempNewData = await format.loadOldData(tempFile);

        if(!tempData){
            console.log("There was no data saved in the data file on server startup");
            console.log(`Wait for initial search`)
            return;
        }
        data = tempData;//, ...tempNewData];

        //seperate into yahoo and tb data
        yahooData = data.filter(el => el.store === 'Yahoo');
        tackleberryData = data.filter(el => el.store === 'Tackleberry');
        console.log(`Loaded ${yahooData.length} Yahoo and ${tackleberryData.length} Tackleberry Listings`);

        return;
    }catch(e){
        return 'Data Initialisation was unsuccessful';
    }
};

const updateData = async () => {
    try{
        //divide data into each store data
        yahooData = data.filter(el => el.store === 'Yahoo');
        tackleberryData = data.filter(el => el.store === 'Tackleberry');
        
         //update Yahoo
         console.log('Updating Yahoo');
         yahooData = await yahoo.updateYahoo(yahooData);
         //format.saveData([...tackleberryData, ...yahooData] , cleanFile);
 
        //Update Tackleberry
        console.log('Updating Tackleberry');
        tackleberryData = await tackleberry.updateTackleberry(tackleberryData);
        //format.saveData([...tackleberryData, ...yahooData] , cleanFile);
        
        //then remove any duplicates based on auctionIDs
        let uniqueData = tools.removeDuplicateObjects([...yahooData, ...tackleberryData], 'auctionID');
        
        return uniqueData;
    }catch(e){
        return e.message;
    }
}

const main = async () => {
    try{
        //Load the last saved lot of data - only once everytime the server starts.
        await initData();
        console.log('INITIALISED');

        //Update the listings with a live scan for a first run
        console.log('CHECKING FOR (INITIAL) UPDATES');
        let tempData = await updateData();
        data = tempData;
        
        //save new data in case of crash
        format.saveData(data , cleanFile);
        //format.deleteData(tempFile);

        firstUpdate = true;
        //update prices
        //data = await scrape.deleteOldListings(data);
        return 'Initialised successfull';
    }
    catch(e){
        return 'Initialised unsuccessfull'
    }
}
// -------------------------------SERVER--------------------------------------------------
// setup express app
var app = express();
const port = process.env.PORT || 3000;

if(!initialised){
    //Async startup sequence that runs once everytime the server starts
    (async () => {
        await main();
    })();
    initialised = true;
}

//setup middleware that processes our body (into JSON)
app.use(bodyParser.json())

//Allow for CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

//Run a scheduled function to scrape the sites
cron.schedule("*/20 * * * *", async function() {
    //Update the listings with a live scan
    if(firstUpdate){
        console.log('CHECKING FOR ROUTINE UPDATES')
        const oldDataLength = data.length;
        let tempData = await updateData();
        data = tempData;
        console.log(`There are: ${data.length - oldDataLength} new Listings`);
        //Only save data if wew haven;t done anything dumb...
        if ((Math.abs(data.length - oldDataLength)) < 200){
            format.saveData(data , cleanFile);
        }else {console.log('Major Data Change');}
    }else{
        console.log('Initial update not completed yet');
    }
});

// ---------------GET LISTINGS--------------------------------------
// This is setting up a response to a request for the /allListings api request. So when the server recieves a www.XXXXX.com/listings HTTP GET request it will send all the data we have currently .
app.get('/allListings', async (req,res) => {
    try{
        //const serverListings = await Listing.find();
        res.send({data});
    }catch (e){
        res.status(400).send(`failed to get send data due to error: ${e}`)
    }
});

//-------------Server Listen Port------------
app.listen(port, () => {
    console.log(`Started on port ${port}`);
})
