var request = require('request-promise');
var cheerio = require('cheerio');
const puppeteer = require('puppeteer');

const format = require('./formatData');
const scraper = require('./scrapeFunctions');


const rootLink = 'http://b-net.tackleberry.co.jp/ec/stk/stk_list.cfm?step=knd&bid=TB0002&ctgry=0004';
const baseUrl = 'http://b-net.tackleberry.co.jp/ec/stk/';
const pageLinkSelector = '#Contents > div > dl > dd > a'
const linkSelectors = {
    general:'#itemtable > tbody > tr', //general listing component
    link : `> td.cell_photo > a`,
    auctionID : ` > td.cell_photo > a > img`,
    price : `> td.cell_price`,
    title : `>td:nth-child(3)` 
 }  
const dataSelectors =  {
    title:'#Contents > form > div > div.detailL > table > tbody > tr:nth-child(4) > td',
    imageUrls: '#Contents > table.photo > tbody > tr > td > a',
    endDate: false,
    startDate: false,
    currentPrice: false,
    buyNowPrice: '#Contents > table.iteminfo > tbody > tr:nth-child(2) > td',
    auctionID: '#Contents > table.photo > tbody > tr > td:nth-child(1) > a > img',
    rating: '#Contents > form > div > div.detailL > table > tbody > tr:nth-child(7) > td > img',
    store: 'TB',
    productDescription: 'body > div > div > div > div > p'  
};
    
const updateTackleberry = async (oldData = []) => {
    try{
    //Enter site specific requests to the scraper system
    let newData = await scraper.update(rootLink, oldData, pageLinkSelector, linkSelectors, baseUrl, requestPageData);
    return newData;
    }catch(e){
        console.log('Had some issue: ', e);
    }
}

module.exports = {
    updateTackleberry
}

//--------------getHTML is a function that uses Puppeteer to scrape site data from dynamic sites
const getHTML = async (browser, link) => {
    try {
        const page = await browser.newPage();
        await page.setRequestInterception(true);
        page.on('request', (request) => {
            if (['image', 'stylesheet', 'font', 'script'].indexOf(request.resourceType()) !== -1) {
                request.abort();
            } else {
                request.continue();
            }
        });
        
        await page.goto(link, { waitUntil: 'networkidle2'});
        let html = await page.content();
        return html;
    } catch (e) {
        return e;
    }
}

//--------------------------------------------------------------
//Requests five listing pages for data at a time (or less than five)
const requestPageData = async (fiveTempLinks) => {
    let startDate, endDate, rating;
    //console.log('We made it to our callback');
    const browser = await puppeteer.launch({
        args: ['--no-sandbox']
      });
    const tempData = await Promise.all(fiveTempLinks.map(async (linkData) =>{
       
        try{    

            const pageHTML = await getHTML(browser, linkData.link);
            //const pageHtml = await request(link);
            
            const $ = cheerio.load(pageHTML);

            //Product title Box
            const tempJapanese = $(dataSelectors.title).text()
            const tempEnglish = await format.translateJPN(tempJapanese);
            const title = format.cleanString(tempEnglish);

            //Product image URLs
            const imageUrls = $(dataSelectors.imageUrls).map((i, url) => {
                return ($(url).find('img').attr('src'));
            }).get();
            
            //Product current price
            const currentPrice = '--'

            //Product buy now price
            let tempEnglish2 = $(dataSelectors.buyNowPrice).text();
            const buyNowPrice = format.processCurrency(tempEnglish2);
            
            //Product  start Date
            startDate = new Date().toString();

            //Product end Date
            endDate = '--';

            //Product auction ID
            const auctionID = `${linkData.auctionID}`;
            
            //Product Description Text
            const tempJapanese2 = $(dataSelectors.productDescription).text();
            //console.log(tempJapanese);
            const tempEnglish3 = await format.translateJPN(tempJapanese2);
            const productDescription = format.cleanString(tempEnglish3);

            //Product Rating
            rating = $(dataSelectors.rating).attr('src');

            //Get Listing time

            let temp = {
                    title: title,
                    linkData: linkData,
                    imageUrls: imageUrls,
                    endDate: endDate,
                    startDate: startDate,
                    currentPrice: currentPrice,
                    buyNowPrice: buyNowPrice,
                    auctionID: auctionID,
                    rating: rating,
                    store: 'Tackleberry',
                    productDescription: productDescription
            };
            return temp;
        }catch (e){
            console.log('error is when trying to load individual listing pages:', e);
            return;
        }
    }));
    //console.log(tempData);
    browser.close();
    return tempData;
}

//Used to test module
// (async () => {
//     let test = await requestPageData(['http://b-net.tackleberry.co.jp/ec/stk/stk_detail.cfm?stkNo=7308123N84567053890123A8&shopID=20276DX983060000DX13&keyS=396110U7503323001V1UP&key1=7295306O61050290008627H3','http://b-net.tackleberry.co.jp/ec/stk/stk_detail.cfm?stkNo=7211123P9456732189012374&shopID=4337700006498319767DS5&keyS=340611J5188203001UMTZ&key1=79952008811184340095631L', 'http://b-net.tackleberry.co.jp/ec/stk/stk_detail.cfm?stkNo=7218123V94567437890123OO&shopID=22476MI98953000044AX&keyS=337911X0189613001DHHF&key1=7751200DP1118539001284O9']);

//     console.log(test[0].productDescription);
// })();